<?php

require_once __DIR__ . '/facebook-php-sdk-v4-5.0-dev/src/Facebook/autoload.php';
require_once __DIR__ . '/config.php';

session_start();

$fb = new Facebook\Facebook($config);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email','publish_actions']; // Optional permissions
$loginUrl = $helper->getLoginUrl($site_url . '/fb-callback.php', $permissions);

header("Location: $loginUrl");